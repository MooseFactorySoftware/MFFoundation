/*--------------------------------------------------------------------------*/
/*   /\/\/\__/\/\/\        MooseFactory Foundation - v1.0.0                    */
/*   \/\/\/..\/\/\/                                                         */
/*        |  |             (c)2007-2016 Tristan Leblanc                     */
/*        (oo)             tristan@moosefactory.eu                          */
/* MooseFactory Software                                                    */
/*--------------------------------------------------------------------------*/

/*
Copyright (c) 2016 Tristan Leblanc - MooseFactory Software <tristan@moosefactory.eu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#ifndef MFTypes_h
#define MFTypes_h

typedef struct {
    char chars[2];
} MFCountryCode;

typedef struct {
    char chars[2];
} MFLanguageCode;

typedef struct {
    char chars[5];
} MFLocaleCode;

typedef struct {
    double longitude;
    double latitude;
    double altitude;
} MFLocation;


typedef NS_OPTIONS(NSUInteger, MFDaySelectorFlag) {
    MFDaysSelector_Monday       = 1,
    MFDaysSelector_Tuesday      = 2,
    MFDaysSelector_Wedenesday   = 4,
    MFDaysSelector_Thursday     = 8,
    MFDaysSelector_Friday       = 16,
    MFDaysSelector_Saturday     = 32,
    MFDaysSelector_Sunday       = 64
};

typedef NSUInteger MFDaysSelectorMask;

#define MFDaysSelectorWorkingDays   ( MFDaysSelector_Monday | MFDaysSelector_Tuesday | MFDaysSelector_Wedenesday | MFDaysSelector_Thursday | MFDaysSelector_Friday )
#define MFDaysSelectorWeekEndDays   ( MFDaysSelector_Saturday | MFDaysSelector_Sunday )
#define MFDaysSelectorEveryDay      ( MFDaysSelectorWorkingDays | MFDaysSelectorWeekEndDays )
#define MFDaysSelectorNoDays        0


#define MF_YesNo(b) ( b ? @"YES" : @"NO" )
#define MF_ValidNotValid(b) ( b ? @"VALID" : @"NOT VALID" )

#endif /* MFTypes_h */
