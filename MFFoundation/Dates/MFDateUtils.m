/*--------------------------------------------------------------------------*/
/*   /\/\/\__/\/\/\        MooseFactory Foundation - v1.0.0                    */
/*   \/\/\/..\/\/\/                                                         */
/*        |  |             (c)2007-2016 Tristan Leblanc                     */
/*        (oo)             tristan@moosefactory.eu                          */
/* MooseFactory Software                                                    */
/*--------------------------------------------------------------------------*/

/*
Copyright (c) 2016 Tristan Leblanc - MooseFactory Software <tristan@moosefactory.eu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#import <MFFoundation/MFDateUtils.h>
#import <MFFoundation/NSDate+MFExtras.h>

@implementation MFDateUtils

#pragma mark - Formatters

+(NSDateFormatter*)rfc3339DateFormatter
{
    static NSDateFormatter* sRFC3339DateFormatter;
    
    if (!sRFC3339DateFormatter) {
        sRFC3339DateFormatter= [[NSDateFormatter alloc] init];
        
        NSLocale* enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH:mm:ssZ"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return sRFC3339DateFormatter;
}

+(NSDateFormatter*)yyyyMMddDateFormatter
{
    static NSDateFormatter* sShortDateFormatter;
    
    if (!sShortDateFormatter) {
        sShortDateFormatter= [[NSDateFormatter alloc] init];
        [sShortDateFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return sShortDateFormatter;
}

#pragma mark - Utils

+(NSDate*)todayStartDate
{
    return [[NSDate date] dayStartDate];
}


+(NSDate*)dateFromYYYYMMDDString:(NSString*)yyyy_mm_dd_String
{
    if (!yyyy_mm_dd_String) return NULL;
    NSUInteger len = yyyy_mm_dd_String.length;
    if ( ( len != 6 ) && ( len !=8 ) ) return NULL;
    NSArray* comps;
    if (len==8) {
        comps = [yyyy_mm_dd_String componentsSeparatedByString:@"-"];
        if (comps.count!=3) {
            comps = [yyyy_mm_dd_String componentsSeparatedByString:@"_"];
            if (comps.count!=3) {
                NSArray* comps = [yyyy_mm_dd_String componentsSeparatedByString:@"/"];
                if (comps.count!=3) return NULL;
            }
        }
    } else {
        comps = [NSArray arrayWithObjects: [yyyy_mm_dd_String substringToIndex:4],
                                           [yyyy_mm_dd_String substringWithRange:NSMakeRange(4, 2)],
                                           [yyyy_mm_dd_String substringFromIndex:6], NULL];
    }
    NSDateComponents* dateComps = [[NSDateComponents alloc] init];
    dateComps.year = [comps[0] integerValue];
    dateComps.month = [comps[1] integerValue];
    dateComps.day = [comps[2] integerValue];
    NSDate *date = [[NSCalendar autoupdatingCurrentCalendar] dateFromComponents:dateComps];
    return date;
}


@end
