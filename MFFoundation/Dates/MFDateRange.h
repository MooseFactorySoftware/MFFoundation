/*--------------------------------------------------------------------------*/
/*   /\/\/\__/\/\/\        MooseFactory Foundation - v1.0.0                    */
/*   \/\/\/..\/\/\/                                                         */
/*        |  |             (c)2007-2016 Tristan Leblanc                     */
/*        (oo)             tristan@moosefactory.eu                          */
/* MooseFactory Software                                                    */
/*--------------------------------------------------------------------------*/

/*
Copyright (c) 2016 Tristan Leblanc - MooseFactory Software <tristan@moosefactory.eu>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 */

#import <Foundation/Foundation.h>

@interface MFDateRange : NSObject



/** Returns a date range with the two given dates.

 @param startDate The start date of the date range.
 @param endDate The end date of the date range.
 @return MFDateRange.
 */

+(id)dateRangeWithStartDate:(NSDate*)startDate endDate:(NSDate*)endDate;



/** Init a given date range with the two given dates.
 
 @param startDate The start date of the date range.
 @param endDate The end date of the date range.
 @return MFDateRange.
 */

-(id)initWithStartDate:(NSDate*)startDate endDate:(NSDate*)endDate;





/** Returns a date range between two times in the given day.
 
 @param dayDate The day where the date range is set.
 @param startHour The start hour of the date range.
 @param startMinute The start minute of the date range.
 @param endHour The end hour of the date range.
 @param endMinute The end minute of the date range.
 @return MFDateRange
 */

+(id)dateRangeWithDayDate:(NSDate*)dayDate startHour:(NSUInteger)startHour startMinute:(NSUInteger)startMinute endHour:(NSUInteger)endHour endMinute:(NSUInteger)endMinute;




/** Inits a given date range between two times in the given day.
 
 @param dayDate The day where the date range is set.
 @param startHour The start hour of the date range.
 @param startMinute The start minute of the date range.
 @param endHour The end hour of the date range.
 @param endMinute The end minute of the date range.
 @return MFDateRange
 */

-(id)initWithDayDate:(NSDate*)dayDate startHour:(NSUInteger)startHour startMinute:(NSUInteger)startMinute endHour:(NSUInteger)endHour endMinute:(NSUInteger)endMinute;




/** Returns YES if the given date is inside the date range.
 
 @param date The date to test.
 @return Boolean
 */

-(BOOL)containsDate:(NSDate*)date;




/** Returns YES if the given date is inside the date range.
 
 @param comps The components of the date
 @param cal The calendar in which the date computation must be set
 @return Boolean
 */

-(BOOL)containsDayWithComponents:(NSDateComponents*)comps inCalendar:(NSCalendar*)cal;





/** Returns the start time of the date range.
 
 @return hours  the hour of the start time
 @return minutes  the minute of the start time
 */

-(void)startHour:(NSUInteger*)hours minutes:(NSUInteger*)minutes;



/** Returns the end time of the date range.
 
 @return hours  the hour of the end time
 @return minutes  the minute of the end time
 */

-(void)endHour:(NSUInteger*)hours minutes:(NSUInteger*)minutes;

-(NSTimeInterval)duration;



/** Returns the end time of the date range.
 
 @return NSDate  the start date of the date range
 */

@property(nonatomic,strong) NSDate* startDate;



/** Returns the end time of the date range.
 
 @return NSDate  the end date of the date range
 */

@property(nonatomic,strong) NSDate* endDate;

@end
