//
//  MFSingularPluralFormatter.h
//  MFFoundation
//
//  Created by Tristan Leblanc on 11/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MFSingularPluralFormatter : NSFormatter

-(NSString*)stringFromInteger:(NSInteger)value;

@property(nonatomic,strong) NSString* singular;
@property(nonatomic,strong) NSString* plural;
@property(nonatomic,strong) NSString* none;

@end


@interface MFTimesFormatter : MFSingularPluralFormatter

+(MFTimesFormatter*)defaultTimesFormatter;
+(MFTimesFormatter*)timesFormatter;

+(NSString*)stringFromInteger:(NSInteger)value;

@end
