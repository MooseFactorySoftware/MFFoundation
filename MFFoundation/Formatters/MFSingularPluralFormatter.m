//
//  MFSingularPluralFormatter.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 11/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <MFFoundation/MFSingularPluralFormatter.h>

static MFTimesFormatter* sDefaultTimesFormatter;

@implementation MFSingularPluralFormatter

+(MFSingularPluralFormatter*)timesFormatter
{
    MFSingularPluralFormatter* newFormatter = [[MFSingularPluralFormatter alloc] init];
    return newFormatter;
}

-(NSString*)stringFromInteger:(NSInteger)value;
{
    if (value==0) return self.none;
    return [NSString stringWithFormat:@"%li %@",(long)value,(value>1 ? self.plural : self.singular)];
}

-(NSString*)plural
{
    if (!_singular && !_plural) return NULL;
    if (!_plural) {
        return [NSString stringWithFormat:@"%@s",_singular];
    }
    return _plural;
}

-(NSString*)none
{
    if (!_singular && !_none) return NULL;
    if (!_none) {
        return [NSString stringWithFormat:@"No %@",_singular];
    }
    return _none;
}

@end





@implementation MFTimesFormatter : MFSingularPluralFormatter

+(MFTimesFormatter*)defaultTimesFormatter {
    if (!sDefaultTimesFormatter) sDefaultTimesFormatter = [[MFTimesFormatter alloc] init];
    return sDefaultTimesFormatter;
}

+(MFTimesFormatter*)timesFormatter
{
    MFTimesFormatter* newFormatter = [[MFTimesFormatter alloc] init];
    return newFormatter;
}

-(id)init
{
    if (self = [super init]) {
        self.singular = @"Time";
        self.plural = @"Times";
        self.none = @"None";
    }
    return self;
}

-(NSString*)stringFromInteger:(NSInteger)value;
{
    if (value==0) return self.none;
    return [NSString stringWithFormat:@"%li %@",(long)value,(value>1 ? self.plural : self.singular)];
}

+(NSString*)stringFromInteger:(NSInteger)value;
{
    return [[MFTimesFormatter defaultTimesFormatter] stringFromInteger:value];
}

@end