//
//  NSDate+MFExtrasFmt.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 11/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "NSDate+MFExtrasFormat.h"

@implementation NSDate (MFExtrasFmt)


#pragma mark - date as string

-(NSString*)dayName
{
    NSCalendar* cal = [NSCalendar autoupdatingCurrentCalendar];
    NSInteger day = [cal component:NSCalendarUnitWeekday fromDate:self];
    return cal.weekdaySymbols[day-1];
}

-(NSString*)shortDayName
{
    NSCalendar* cal = [NSCalendar autoupdatingCurrentCalendar];
    NSInteger day = [cal component:NSCalendarUnitWeekday fromDate:self];
    return cal.shortWeekdaySymbols[day-1];
}

-(NSString*)dateTimeStringWithDay
{
    return [NSString stringWithFormat:@"%@ %@", self.dayName, [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterMediumStyle]];
}


-(NSString*)dateStringWithDay
{
    return [NSString stringWithFormat:@"%@ %@", self.dayName, [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle]];
}



-(NSString*)shortDateStringWithDay
{
    return [NSString stringWithFormat:@"%@ %@", self.shortDayName, [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle]];
}


-(NSString*)dateTimeString
{
    return [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterMediumStyle];
}


-(NSString*)dateString
{
    return [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
}


-(NSString*)shortDateString
{
    return [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterNoStyle];
}


-(NSString*)unixDateString
{
    return [self rfc3339String];
}


-(NSString*)timeString
{
    return [NSDateFormatter localizedStringFromDate:self dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterMediumStyle];
}


-(NSString*)rfc3339String
{
    static NSDateFormatter* sRFC3339DateFormatter;
    
    if (!sRFC3339DateFormatter) {
        sRFC3339DateFormatter= [[NSDateFormatter alloc] init];
        
        NSLocale* enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
        
        [sRFC3339DateFormatter setLocale:enUSPOSIXLocale];
        [sRFC3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH:mm:ssZ"];
        [sRFC3339DateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    return [sRFC3339DateFormatter stringFromDate:self];
}


-(NSString*)yyyyMMddString
{
    static NSDateFormatter* syyyyMMddFormatter;
    
    if (!syyyyMMddFormatter) {
        syyyyMMddFormatter= [[NSDateFormatter alloc] init];
        [syyyyMMddFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    return [syyyyMMddFormatter stringFromDate:self];
}

@end
