//
//  NSDate+MFExtrasFmt.h
//  MFFoundation
//
//  Created by Tristan Leblanc on 11/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (MFExtrasFormat)

/*!
 @discussion This category add convenience formatting methods to the NSDate class.
 
 ## Version information
 
 __Version__: 1.0
 
 __Last update__: 2016/03/19
 
 __Developer__:  Tristan Leblanc - MooseFactory Software.
 
 ## Methods
 
 */



/** Returns the  day name of the target date, in the system calendar.
 @return NSString */

-(NSString*)dayName;




/** Returns the abreviated day name of the target date, in the system calendar.
 @return NSString */

-(NSString*)shortDayName;



/** Returns the full date ( with day name and time ) as a string.
 @return NSString */

-(NSString*)dateTimeStringWithDay;





/** Returns the full date ( with day name ) as a string.
 @return NSString */

-(NSString*)dateStringWithDay;




/** Returns the date ( with day name ) as a string.
 @return NSString */

-(NSString*)shortDateStringWithDay;




/** Returns the full date ( abrev month and time ) as a string.
 @return NSString */


-(NSString*)dateTimeString;




/** Returns the date ( abrev month ) as a string.
 @return NSString */

-(NSString*)dateString;




/** Returns the short date ( numeric ) as a string.
 @return NSString */

-(NSString*)shortDateString;




/** Returns the time as a string.
 @return NSString */

-(NSString*)timeString;




/** Returns the date  formatted as a unix string ( RFC3339 ).
 @return NSString */

-(NSString*)rfc3339String;




/** Returns the date as a string YYYY-MM-DD.
 @return NSString */

-(NSString*)yyyyMMddString;

@end
