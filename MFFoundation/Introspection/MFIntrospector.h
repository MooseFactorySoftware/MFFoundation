//
//  MFIntrospector.h
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject ( MFExtras_Introspection )

+(NSArray*)vars;
+(NSArray*)properties;
+(NSArray*)methods;

@end

@interface MFIntrospector : NSObject

+(NSArray*)varsForClass:(Class)aClass;
+(NSArray*)propertiesForClass:(Class)aClass;
+(NSArray*)methodsForClass:(Class)aClass;

+(NSDictionary*)dumpClass:(Class)class;

+(NSString*)dumpClassAsString:(Class)class;

@end
