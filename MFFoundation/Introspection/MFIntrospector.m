//
//  MFIntrospector.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFIntrospector.h"
#import <objc/runtime.h>



@implementation NSObject ( MFExtras_Introspection )

+(NSArray*)vars
{
    return [MFIntrospector varsForClass:[self class]];
}

+(NSArray*)properties
{
    return [MFIntrospector propertiesForClass:[self class]];
}

+(NSArray*)methods
{
    return [MFIntrospector methodsForClass:[self class]];
}

@end



@implementation MFIntrospector

+(NSArray*)varsForClass:(Class)aClass
{
    u_int count;
    
    Ivar* ivars = class_copyIvarList(aClass, &count);
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        const char* ivarName = ivar_getName(ivars[i]);
        [array addObject:[NSString  stringWithCString:ivarName encoding:NSUTF8StringEncoding]];
    }
    free(ivars);
    return [NSArray arrayWithArray:array];
}

+(NSArray*)propertiesForClass:(Class)aClass
{
    u_int count;
    
    objc_property_t* properties = class_copyPropertyList(aClass, &count);
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        const char* propertyName = property_getName(properties[i]);
        [array addObject:[NSString  stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
    }
    free(properties);
    return [NSArray arrayWithArray:array];
}

+(NSArray*)methodsForClass:(Class)aClass
{
    u_int count;
    
    Method* methods = class_copyMethodList(aClass, &count);
    NSMutableArray* array = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i < count ; i++)
    {
        SEL selector = method_getName(methods[i]);
        const char* methodName = sel_getName(selector);
        [array addObject:[NSString  stringWithCString:methodName encoding:NSUTF8StringEncoding]];
    }
    free(methods);
    return [NSArray arrayWithArray:array];
}

+(NSDictionary*)dumpClass:(Class)aClass
{
    NSDictionary* classDump = [NSDictionary dictionaryWithObjectsAndKeys:
                               [MFIntrospector varsForClass:aClass], @"vars",
                               [MFIntrospector propertiesForClass:aClass], @"properties",
                               [MFIntrospector methodsForClass:aClass], @"methods",
                               nil];
    
    return classDump;
}

+(NSString*)dumpClassAsString:(Class)class
{
    return [MFIntrospector dumpClass:class].description;
}

@end
