//
//  MFFoundation_Tests_MacOS.m
//  MFFoundation Tests MacOS
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <XCTest/XCTest.h>

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

#import "MFDate_Tester.h"
#import "NSArray_Tester.h"
#import "NSData_Tester.h"
#import "NSString_Tester.h"
#import "MFMaths_Tester.h"
#import "MFSystem_Tester.h"
#import "MFFileSystem_Tester.h"
#import "MFDateRange_Tester.h"

@interface MFFoundation_Tests : XCTestCase

@end

@implementation MFFoundation_Tests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void)testDates
{
    [MFDate_Tester test];
    [MFDate_Tester testFormats];
}

-(void)testMFSystem
{
    [MFSystem_Tester test];
}

-(void)testMFMaths
{
    [MFMaths_Tester test];
    [MFMaths_Tester testFastTrigo];
}

-(void)testMFDataExtras
{
    [NSData_Tester test];
}

-(void)testMFStringExtras
{
    [NSString_Tester test];
}

-(void)testNSArray_MFExtras
{
    [NSArray_Tester test];
}

-(void)testNSFileSystem_MFExtras
{
    [MFFileSystem_Tester test];
}

-(void)testDateRange
{
    [MFDateRange_Tester test];
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
