//
//  MFTestLogger.h
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MFTestLogger : NSObject

+(NSString*)logClassTestHeader:(NSString*)className;

+(NSString*)logFunctionTestHeader:(NSString*)functionName;

+(NSString*)logClassTestFooter:(NSString*)className;

@end

@interface NSMutableString (MFTestLogger)

-(void)appendClassHeader:(NSString*)className;
-(void)appendClassFooter:(NSString*)className;
-(void)appendFunctionHeader:(NSString*)funcName;
-(void)append:(NSString*)format,...;


@end