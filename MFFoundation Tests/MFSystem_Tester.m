//
//  MFSystem_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFSystem_Tester.h"


#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation MFSystem_Tester

+(void)test
{
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"MFSystem"];

    [logStr appendFunctionHeader: @"Hardware :"];
    [logStr append:@"[MFSystem machine] = %@",[MFSystem machine]];
    [logStr append:@"[MFSystem model] = %@",[MFSystem model]];
    [logStr append:@"[MFSystem cpu] = %@",[MFSystem cpu]];
    [logStr append:@"[MFSystem byteorder] = %ldl",(long)[MFSystem byteorder]];
    [logStr append:@"[MFSystem architecture] = %@",[MFSystem architecture]];
    
    [logStr appendFunctionHeader: @"Kernel :"];
    [logStr append:@"[MFSystem kernelVersion] = %@",[MFSystem kernelVersion]];
    [logStr append:@"[MFSystem kernelRelease] = %@",[MFSystem kernelRelease]];
    [logStr append:@"[MFSystem kernelRevision] = %@",[MFSystem kernelRevision]];
    
    [logStr appendFunctionHeader: @"System :"];
    [logStr append:@"[MFSystem systemVersion] = %@",[MFSystem systemVersion]];
    [logStr append:@"[MFSystem systemVersionFloat] = %f",[MFSystem systemVersionFloat]];
    [logStr append:@"[MFSystem systemVersionPatch] = %@",[MFSystem systemVersionPatch]];
    [logStr append:@"[MFSystem systemVersionPatchFloat] = %f",[MFSystem systemVersionPatchFloat]];
    
    [logStr appendFunctionHeader: @"Test :"];
    [logStr append:@"[MFSystem systemVersionPriorTo:@\"8.0\"] = %@",[MFSystem systemVersionPriorTo:@"8.0"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorOrEqualTo:@\"8.0\"] = %@",[MFSystem systemVersionPriorOrEqualTo:@"8.0"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorTo:@\"8.5\"] = %@",[MFSystem systemVersionPriorTo:@"8.5"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorOrEqualTo:@\"8.5\"] = %@",[MFSystem systemVersionPriorOrEqualTo:@"8.5"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorTo:@\"9.0\"] = %@",[MFSystem systemVersionPriorTo:@"9.0"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorOrEqualTo:@\"9.0\"] = %@",[MFSystem systemVersionPriorOrEqualTo:@"9.0"] ? @"YES" : @"NO"];
    
    [logStr append:@"[MFSystem systemVersionPriorTo:@\"20.0\"] = %@   ( Big Enough ??? ;)",[MFSystem systemVersionPriorTo:@"20.0"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorOrEqualTo:@\"20.0\"] = %@",[MFSystem systemVersionPriorOrEqualTo:@"20.0"] ? @"YES" : @"NO"];
    
    NSString* version = [MFSystem systemVersion];
    
    [logStr append:@"[MFSystem systemVersionPriorTo:@\"%@\"] = %@",version,[MFSystem systemVersionPriorTo:@"9.0"] ? @"YES" : @"NO"];
    [logStr append:@"[MFSystem systemVersionPriorOrEqualTo:@\"%@\"] = %@",version,[MFSystem systemVersionPriorOrEqualTo:version] ? @"YES" : @"NO"];
    
    [logStr appendClassFooter:@"MFSystem"];
    
    NSLog(@"%@",logStr);
}

@end
