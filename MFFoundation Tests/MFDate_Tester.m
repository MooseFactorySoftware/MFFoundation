//
//  MFDate_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFDate_Tester.h"

#import <XCTest/XCTest.h>

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation MFDate_Tester

+(void)test
{
    NSDate* todayDate = [NSDate date];
    NSDate* todayDateInOneHour = [[NSDate date] dateByAddingTimeInterval:60*60];
    NSDate* tomorrowDate = [[NSDate date] dateByAddingTimeInterval:60*60*24];
    
    NSDate* dateInOneMonth = [[NSDate date] dateByAddingYears:0 months:1 days:0];
    NSDate* dateInOneYear = [[NSDate date] dateByAddingYears:1 months:0 days:0];
    
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"NSDate+MFExtras"];
    
    //[logStr append:[MFIntrospector dumpClassAsString:[NSDate class]]];
    
    [logStr append:@"todayDate : \r%@",[todayDate dateTimeStringWithDay]];
    [logStr append:@"todayDateInOneHour : \r%@",[todayDateInOneHour dateTimeStringWithDay]];
    [logStr append:@"tomorrowDate : \r%@",[tomorrowDate dateTimeStringWithDay]];
    [logStr append:@"dateInOneMonth : \r%@",[dateInOneMonth dateTimeStringWithDay]];
    [logStr append:@"dateInOneYear : \r%@",[dateInOneYear dateTimeStringWithDay]];
    
    [logStr appendFunctionHeader:@"Setters"];
    
    [logStr append:@"[todayDate dateByClearingTime] :\r%@",[todayDate dateByClearingTime].dateTimeStringWithDay];
    [logStr append:@"\r[todayDate dateBySettingHour:8 minute:30] :\r%@",[todayDate dateBySettingHour:8 minute:30].dateTimeStringWithDay];
    
    [logStr appendFunctionHeader:@"Comparison Methods"];
    
    [logStr append:@"[todayDate isSameDayAsDate:tommorowDate] :\r%@",[todayDate isSameDayAsDate:tomorrowDate] ? @"YES" : @"NO"];
    [logStr append:@"[todayDate isSameDayAsDate:todayDateInOneHour] :\r%@",[todayDate isSameDayAsDate:todayDateInOneHour] ? @"YES" : @"NO"];
    [logStr append:@"[todayDate isSameMonthAsDateDate:tommorowDate] :\r%@",[todayDate isSameMonthAsDate:tomorrowDate] ? @"YES" : @"NO"];
    [logStr append:@"[todayDate isSameMonthAsDateDate:dateInOneYear] :\r%@",[todayDate isSameMonthAsDate:dateInOneYear] ? @"YES" : @"NO"];
    
    [logStr appendFunctionHeader:@"Shift Dates"];
    
    [logStr append:@"[todayDate oneMonthLater] :\r%@",[todayDate oneMonthLater].dateTimeStringWithDay];
    [logStr append:@"[todayDate oneMonthEarlier] :\r%@",[todayDate oneMonthEarlier].dateTimeStringWithDay];
    [logStr append:@"[todayDate oneDayEarlier] :\r%@",[todayDate oneDayEarlier].dateTimeStringWithDay];
    [logStr append:@"[todayDate oneDayLater] :\r%@",[todayDate oneDayLater].dateTimeStringWithDay];
    
    [logStr append:@"[todayDate dateByAddingYears:4 month:3 day:7] :\r%@",[todayDate dateByAddingYears:4 months:3 days:7].dateTimeStringWithDay];
    
    [logStr appendFunctionHeader:@"Seconds"];
    
    [logStr append:@"[todayDate secondsSinceStartOfDay] :\r%lu",(unsigned long)[todayDate secondsSinceStartOfDay]];
    [logStr append:@"[todayDate millisecondsSinceStartOfDay] :\r%lu",(unsigned long)[todayDate millisecondsSinceStartOfDay]];
    
    [logStr appendFunctionHeader:@"Components"];
    
    [logStr append:@"[todayDate components] :\r%@",[todayDate components]];
    
    [logStr appendFunctionHeader:@"Start Dates"];
    
    [logStr append:@"[todayDate dayStartDate] :\r%@",[todayDate dayStartDate].dateTimeStringWithDay];
    [logStr append:@"[todayDate previousDayStartDate] :\r%@",[todayDate previousDayStartDate].dateTimeStringWithDay];
    [logStr append:@"[todayDate weekStartDate] :\r%@",[todayDate weekStartDate].dateTimeStringWithDay];
    [logStr append:@"[todayDate previousWeekStartDate] :\r%@",[todayDate previousWeekStartDate].dateTimeStringWithDay];
    
    [logStr appendClassFooter:@"NSDate+MFExtras"];
    
    NSLog(@"%@",logStr);
    
}

+(void)testFormats
{
    NSDate* todayDate = [NSDate date];
    
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"NSDate+MFExtrasFormat"];
    
    [logStr append:@"[todayDate dayName] : \r%@",[todayDate dayName]];
    [logStr append:@"[todayDate shortDayName] : \r%@",[todayDate shortDayName]];
    [logStr append:@"[todayDate dateStringWithDay] : \r%@",[todayDate dateStringWithDay]];
    [logStr append:@"[todayDate shortDateStringWithDay] : \r%@",[todayDate shortDateStringWithDay]];

    [logStr append:@"[todayDate dateTimeStringWithDay] : \r%@",[todayDate dateTimeStringWithDay]];
    [logStr append:@"[todayDate dateTimeString] : \r%@",[todayDate dateTimeString]];
    [logStr append:@"[todayDate dateString] : \r%@",[todayDate dateString]];
    [logStr append:@"[todayDate shortDateString] : \r%@",[todayDate shortDateString]];
    [logStr append:@"[todayDate timeString] : \r%@",[todayDate timeString]];
    [logStr append:@"[todayDate rfc3339String] : \r%@",[todayDate rfc3339String]];
    [logStr append:@"[todayDate yyyyMMddString] : \r%@",[todayDate yyyyMMddString]];

    
    [logStr appendClassFooter:@"NSDate+MFExtrasFormat"];
    
    NSLog(@"%@",logStr);
    
}

@end
