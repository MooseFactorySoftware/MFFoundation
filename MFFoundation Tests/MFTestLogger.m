//
//  MFTestLogger.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFTestLogger.h"

@implementation MFTestLogger

+(NSString*)logClassTestHeader:(NSString*)className
{
    return [NSString stringWithFormat:@"\r\r\
********************************************************************************\r\r\
TestClass : %@\r\r\
********************************************************************************\
\r",className];
}

+(NSString*)logFunctionTestHeader:(NSString*)functionName
{
return [NSString stringWithFormat:@"\r\r\
--------------------------------------------------------------------------------\r\
Test Class : %@\r",functionName];
}

+(NSString*)logClassTestFooter:(NSString*)functionName
{
    return [NSString stringWithFormat:@"\r\r********************************************************************************\r\
'%@' Test Done\r********************************************************************************\r\r",functionName];
}

@end

@implementation NSMutableString (MFTestLogger)

-(void)appendClassHeader:(NSString*)className
{
    [self appendString:[MFTestLogger logClassTestHeader:className]];
}

-(void)appendClassFooter:(NSString*)className
{
    [self appendString:[MFTestLogger logClassTestFooter:className]];
}


-(void)appendFunctionHeader:(NSString*)funcName
{
    [self appendString:[MFTestLogger logFunctionTestHeader:funcName]];
}

-(void)append:(NSString*)format, ...
{
    va_list argumentList;
    va_start(argumentList, format);
    NSString *string = [[NSString alloc] initWithFormat:format arguments:argumentList];
    va_end(argumentList);
    [self appendFormat:@"\r%@\r",string];
}


@end