//
//  MFFileSystem_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFFileSystem_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation MFFileSystem_Tester


+(NSString*)testPath:(NSString*)path
{
    NSFileManager* fm = [NSFileManager defaultManager];
    
    NSMutableString* logStr = [NSMutableString string];
    [logStr append:@"Test Path : %@",path];

    BOOL dirEmpty,dirEmptyRec;
    BOOL onlyInvisible,onlyInvisibleRec,gotInvisibleFolders;
    
    BOOL testFolderExists;
    BOOL testFolderIsFolder;

    dirEmpty = [fm isPathDirectoryEmpty:path isDirectory:&testFolderIsFolder fileExists:&testFolderExists];
    
    [logStr append:@"[fm isPathDirectoryEmpty:path isDirectory:&testFolderIsFolder fileExists:&testFolderExists]",path];
    [logStr append:@"   Exists : %@   Is folder : %@   Empty : %@",(testFolderExists||testFolderIsFolder) ? @"YES" : @"NO", testFolderIsFolder ? @"YES" : @"NO", dirEmpty ? @"YES" : @"NO"];

    dirEmptyRec = [fm isPathDirectoryEmpty:path isDirectory:&testFolderIsFolder fileExists:&testFolderExists recursive:YES];

    [logStr append:@"[fm isPathDirectoryEmpty:path isDirectory:&testFolderIsFolder fileExists:&testFolderExists recursive:YES]",path];
    [logStr append:@"   Exists : %@   Is folder : %@   Empty : %@",(testFolderExists||testFolderIsFolder) ? @"YES" : @"NO", testFolderIsFolder ? @"YES" : @"NO", dirEmpty ? @"YES" : @"NO"];
    
    onlyInvisible = [fm directoryContainsOnlyInvisibleFiles:path containsInvisibleFolders:&gotInvisibleFolders];
    [logStr append:@"[fm directoryContainsOnlyInvisibleFiles:path directoryContainsOnlyInvisibleFiles:path containsInvisibleFolders:&gotInvisibleFolders]",path];
    [logStr append:@"   Contains Only Invisibles : %@     Invisible Folders : %@",onlyInvisible ? @"YES" : @"NO", gotInvisibleFolders ? @"YES" : @"NO"];
    

    onlyInvisibleRec = [fm directoryContainsOnlyInvisibleFiles:path containsInvisibleFolders:&gotInvisibleFolders recursive:YES];
    [logStr append:@"[fm directoryContainsOnlyInvisibleFiles:path directoryContainsOnlyInvisibleFiles:path containsInvisibleFolders:&gotInvisibleFolders recursive:YES]",path];
    [logStr append:@"   Contains Only Invisibles : %@     Invisible Folders : %@",onlyInvisible ? @"YES" : @"NO", gotInvisibleFolders ? @"YES" : @"NO"];

    
    return logStr;
}

+(void)test
{
    NSFileManager* fm = [NSFileManager defaultManager];
    NSMutableString* logStr = [NSMutableString string];
    [logStr appendClassHeader:@"NSFileSystem_MFExtras Will Pass Test"];

    NSString* path = @"~/NSFileSystem_MFExtras-TestFolder";
    NSString* filesPath = [path stringByAppendingPathComponent:@"FolderWithFiles"];
    NSString* invisibleFilesPath = [path stringByAppendingPathComponent:@"FolderWithInvisibleFilesOnly"];
    NSString* emptyPath = [path stringByAppendingPathComponent:@"EmptyFolder"];
    NSString* folderWithFolderAndFilesPath = [path stringByAppendingPathComponent:@"FolderWithFolderAndFiles"];

        [logStr append:[MFFileSystem_Tester testPath:filesPath]];
        [logStr append:[MFFileSystem_Tester testPath:invisibleFilesPath]];
        [logStr append:[MFFileSystem_Tester testPath:emptyPath]];
        [logStr append:[MFFileSystem_Tester testPath:folderWithFolderAndFilesPath]];
        
    [logStr appendClassFooter:@"NSFileSystem_MFExtras"];

    NSLog(@"%@",logStr);
}
@end
