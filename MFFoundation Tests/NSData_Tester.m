//
//  NSData_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "NSData_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation NSData_Tester

+(void)test
{
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"NSString+MFExtras"];

    
    [logStr appendFunctionHeader:@"Bytes"];
    
    NSString* str = @"0102030405AA00CDEF";
    
    NSData* data = [NSData dataWithBytesString:str];
        
    [logStr append:@"data = [NSData dataWithBytesString:@\"0102030405AA00CDEF\"] = %@",data.description];
    [logStr append:@"[data bytesString] = \"%@\"",[data bytesString]];
    
    [logStr appendFunctionHeader:@"Random"];
    
    data = [NSData randomDataOfLength:40];
    [logStr append:@"data = [NSData randomDataOfLength:40] = %@",data.description];


    [logStr appendClassFooter:@"NSString+MFExtras"];
    
    NSLog(@"%@",logStr);
    
}

@end
