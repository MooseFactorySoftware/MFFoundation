//
//  MFMaths_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFMaths_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"


@implementation MFMaths_Tester

+(void)test
{
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"MFMathUtilities"];
    
    [logStr appendFunctionHeader:@"Constants"];
    
    [logStr append:@"kPiOnThree = %f",MF_PiOnThree];
    [logStr append:@"kTwoPiOnThree = %f",MF_TwoPiOnThree];
    
    [logStr appendFunctionHeader:@"Clamp"];
    [logStr append:@"\rMF_clampOne(0.6f) = %f",MF_clampOne(0.6f)];
    [logStr append:@"MF_clampOne(1.8f) = %f",MF_clampOne(1.8f)];
    [logStr append:@"MF_clampOne(-0.6f) = %f",MF_clampOne(-0.6f)];
    [logStr append:@"MF_clampOne(-1.8f) = %f",MF_clampOne(-1.8f)];
    
    [logStr append:@"\rMF_clampAngle(2.0f) = %f",MF_clampAngle(2.0f)];
    [logStr append:@"MF_clampAngle(7.8f) = %f",MF_clampAngle(7.8f)];
    [logStr append:@"MF_clampAngle(-2.0f) = %f",MF_clampAngle(-2.0f)];
    [logStr append:@"MF_clampAngle(-7.8f) = %f",MF_clampAngle(-7.8f)];
    
    [logStr appendFunctionHeader:@"Mod"];
    [logStr append:@"\rMF_modf(3.5f,1.4f) = %f",MF_modf(3.5f,1.4f)];
    [logStr append:@"MF_modf(3.5f,2.9f) = %f",MF_modf(3.5f,2.9f)];
    [logStr append:@"MF_modf(3.5f,4.0f) = %f",MF_modf(3.5f,4.0f)];
    
    [logStr append:@"MF_modf(-3.5f,-1.4f) = %f",MF_modf(-3.5f,-1.4f)];
    [logStr append:@"MF_modf(-3.5f,-2.9f) = %f",MF_modf(-3.5f,-2.9f)];
    [logStr append:@"MF_modf(-3.5f,-4.0f) = %f",MF_modf(-3.5f,-4.0f)];
    
    [logStr append:@"\rmfpcos(0.0f) = %f",MF_pcos(0.0f)];
    [logStr append:@"mfpcos(M_PI / 2) = %f",MF_pcos(M_PI / 2)];
    [logStr append:@"mfpcos(M_PI) = %f",MF_pcos(M_PI)];
    [logStr append:@"mfpcos(M_PI*1.5) = %f",MF_pcos(M_PI*1.5)];
    
    [logStr appendFunctionHeader:@"Min/Max"];
    [logStr append:@"\rMF_maxf(3.5f,1.4f) = %f",MF_maxf(3.5f,1.4f)];
    [logStr append:@"MF_maxf(3.5f,-2.9f) = %f",MF_maxf(3.5f,-2.9f)];
    [logStr append:@"MF_maxf(-3.5f,-4.0f) = %f",MF_maxf(-3.5f,-4.0f)];
    
    [logStr append:@"\rMF_minf(3.5f,1.4f) = %f",MF_minf(3.5f,1.4f)];
    [logStr append:@"MF_minf(3.5f,-2.9f) = %f",MF_minf(3.5f,-2.9f)];
    [logStr append:@"MF_minf(-3.5f,-4.0f) = %f",MF_minf(-3.5f,-4.0f)];
    
    [logStr append:@"\rMF_max3f(-3.5f,-1.4f,8.0f) = %f",MF_max3f(-3.5f,-1.4f,8.0f)];
    [logStr append:@"MF_max3f(-3.5f,2.9f,-1.0f) = %f",MF_max3f(-3.5f,2.9f,-1.0f)];
    [logStr append:@"MF_max3f(3.5f,4.0f,13.0f) = %f",MF_max3f(3.5f,4.0f,13.0f)];
    
    [logStr append:@"\rMF_min3f(-3.5f,-1.4f,8.0f) = %f",MF_min3f(-3.5f,-1.4f,8.0f)];
    [logStr append:@"MF_min3f(-3.5f,-20.9f,-1.0f) = %f",MF_min3f(-3.5f,-20.9f,-1.0f)];
    [logStr append:@"MF_min3f(13.5f,4.0f,1.0f) = %f",MF_min3f(13.5f,4.0f,1.0f)];
    
    [logStr appendFunctionHeader:@"Trigo"];
    [logStr append:@"\rmfpsin(0.0f) = %f",MF_psin(0.0f)];
    [logStr append:@"mfpsin(M_PI / 2) = %f",MF_psin(M_PI / 2)];
    [logStr append:@"mfpsin(M_PI) = %f",MF_psin(M_PI)];
    [logStr append:@"mfpsin(M_PI*1.5) = %f",MF_psin(M_PI*1.5)];

    double a,b;
    MF_cartesianToPolar(4,3,&a,&b);
    [logStr append:@"\rMF_cartesianToPolar(4,3) : teta=%f, phi=%f",a,b];
    MF_cartesianToPolar(-4,3,&a,&b);
    [logStr append:@"MF_cartesianToPolar(-4,3) : teta=%f, phi=%f",a,b];
    MF_cartesianToPolar(-4,-3,&a,&b);
    [logStr append:@"MF_cartesianToPolar(-4,-3) : teta=%f, phi=%f",a,b];
    MF_cartesianToPolar(4,-3,&a,&b);
    [logStr append:@"MF_cartesianToPolar(4,-3) : teta=%f, phi=%f",a,b];
    
    [logStr appendClassFooter:@"MFMathUtilities"];
    
    NSLog(@"%@",logStr);
}

+(void)testFastTrigo
{
    
    buildMFFastTrigoTables();
    
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"MFFastTrigo"];
    
    [logStr appendFunctionHeader:@"Trigo"];

    [logStr append:@"mffsind(0)=%.2f",mffsind(0)];
    [logStr append:@"mffsind(30)=%.2f",mffsind(30)];
    [logStr append:@"mffsind(45)=%.2f",mffsind(45)];
    [logStr append:@"mffsind(90)=%.2f",mffsind(90)];
    [logStr append:@"mffsind(180)=%.2f",mffsind(180)];
    
    [logStr append:@"mffcosd(0)=%.2f",mffcosd(0)];
    [logStr append:@"mffcosd(30)=%.2f",mffcosd(30)];
    [logStr append:@"mffcosd(45)=%.2f",mffcosd(45)];
    [logStr append:@"mffcosd(90)=%.2f",mffcosd(90)];
    [logStr append:@"mffcosd(180)=%.2f",mffcosd(180)];

    
    [logStr appendFunctionHeader:@"Performance Test"];

    [logStr appendFunctionHeader:@"Compute 10000 sin(double) - <math.h>"];
    
    NSDate *start=[NSDate date];
    
    
        for (int i=0;i<30000;i++) {
            sin( 1.923f );
        }
    
    CGFloat seconds = [[NSDate date] timeIntervalSinceDate:start];
    
    [logStr append:@"** Elapsed Time : %.3f",seconds*1000];
    [logStr appendFunctionHeader:@"Compute 10000 mffsind(long)"];
    
    start=[NSDate date];
        for (int i=0;i<30000;i++) {
            mffsind( 30 );
        }
    
    seconds = [[NSDate date] timeIntervalSinceDate:start];
    
    [logStr append:@"** Elapsed Time : %.3f",seconds*1000];

    [logStr appendClassFooter:@"MFFastTrigo"];
    
    NSLog(@"%@",logStr);
}

@end
