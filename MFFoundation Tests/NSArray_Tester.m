//
//  NSArray_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "NSArray_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation NSArray_Tester

+(void)test
{
    NSArray* testArray = @[@"Zebra",@"Cat",@"Lion",@"Dog",@"Camel"];
    
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"NSArray+MFExtras"];
    
    [logStr append:@"testArray = \r%@",[testArray description]];
    
    [logStr appendFunctionHeader:@"Removing Objects :"];
    [logStr append:@"[testArray arrayByRemovingFirstObject] :\r%@",[testArray arrayByRemovingFirstObject]];
    [logStr append:@"[testArray arrayByRemovingLastObject] :\r%@",[testArray arrayByRemovingLastObject]];
    
    [logStr appendFunctionHeader:@"Ordering"];
    [logStr append:@"[testArray alphabeticallySortedArray] :\r%@",[testArray alphabeticallySortedArray]];
    [logStr append:@"[sortedArray reversedArray] :\r%@",[[testArray alphabeticallySortedArray] reversedArray]];
    
    [logStr appendClassFooter:@"NSArray+MFExtras"];
    
    NSLog(@"%@",logStr);
    
}

@end
