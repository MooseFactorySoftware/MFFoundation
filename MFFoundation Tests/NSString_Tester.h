//
//  NSString_Tester.h
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface NSString_Tester : XCTestCase

+(void)test;

@end
