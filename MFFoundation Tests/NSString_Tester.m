//
//  NSString_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "NSString_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation NSString_Tester

+(void)test
{
    NSMutableString* logStr = [NSMutableString string];
    
    [logStr appendClassHeader:@"NSString+MFExtras"];
    
    
    [logStr appendFunctionHeader:@"UUID :"];
    [logStr append:@"[NSString UUIDString] = %@",[NSString UUIDString]];
    
    [logStr appendFunctionHeader:@"Crypting :"];
    [logStr append:@"[@\"MooseFactory\" md5] = %@",[@"MooseFactory" md5]];
    [logStr append:@"[@\"MooseFactory\" sha1] = %@",[@"MooseFactory" sha1]];
    
    [logStr appendFunctionHeader:@"Validation :"];
    [logStr append:@"[@\"MooseFactory\" isValidEmail:YES] = %@",[@"MooseFactory" isValidEmail:YES] ? @"VALID" : @"NOT VALID"];
    [logStr append:@"[@\"moose@moosefactory\" isValidEmail:YES] = %@",[@"moose@moosefactory" isValidEmail:YES] ? @"VALID" : @"NOT VALID"];
    [logStr append:@"[@\"moose@moosefactory.eu\" isValidEmail:YES] = %@",[@"moose@moosefactory.eu" isValidEmail:YES] ? @"VALID" : @"NOT VALID"];
    [logStr append:@"[@\"moo+se@moosefactory.eu\" isValidEmail:YES] = %@",[@"moo+se@moosefactory.eu" isValidEmail:YES] ? @"VALID" : @"NOT VALID"];
    [logStr append:@"[@\"moo+se@moosefactory.eu\" isValidEmail:NO] = %@",[@"moo+se@moosefactory.eu" isValidEmail:NO] ? @"VALID" : @"NOT VALID"];
    
    [logStr appendFunctionHeader:@"Percent Escape :"];
    
    NSString* string = @"Let's all shout \"Moose & MooseFactory\" !";
    NSString* encodedStr = [string urlEncode];
    NSString* decodedStr = [encodedStr urlDecode];
    
    [logStr append:@"[@\"%@\" urlEncode] = %@",string,encodedStr];
    [logStr append:@"[@\"%@\" urlDecode] = %@",encodedStr,decodedStr];
    
    [logStr appendClassFooter:@"NSString+MFExtras"];
    
    NSLog(@"%@",logStr);
    
}

@end
