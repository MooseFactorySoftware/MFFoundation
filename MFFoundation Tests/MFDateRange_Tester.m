//
//  MFDateRange_Tester.m
//  MFFoundation
//
//  Created by Tristan Leblanc on 12/06/16.
//  Copyright © 2016 MooseFactory. All rights reserved.
//

#import "MFDateRange_Tester.h"

#import <MFFoundation/MFFoundation.h>

#import "MFTestLogger.h"

@implementation MFDateRange_Tester

+(void)test
{
    NSDate* start = [[NSDate date] dateByAddingYears:0 months:0 days:-4];
    NSDate* end = [[NSDate date] dateByAddingYears:0 months:0 days:2];
    NSDate* today = [NSDate date];
    
    start = [start dateBySettingHour:8 minute:30];
    end = [end dateBySettingHour:17 minute:15];
    
    MFDateRange* dr = [MFDateRange dateRangeWithStartDate:start endDate:end];
    MFDateRange* dr2 = [MFDateRange dateRangeWithDayDate:today startHour:9 startMinute:0 endHour:13 endMinute:30];
    
    NSMutableString* logStr = [NSMutableString string];

    [logStr appendClassHeader:@"MFDateRange"];
    
    [logStr append:@"Start Date : %@",[start dateTimeStringWithDay]];
    [logStr append:@"End Date : %@",[end dateTimeStringWithDay]];
    [logStr append:@"Today : %@",[today dateTimeStringWithDay]];
    
    [logStr appendFunctionHeader:@"Creation"];

    [logStr append:@"a_daterange = [MFDateRange dateRangeWithStartDate:start endDate:end]"];

    [logStr append:@"a_daterange_today = [MFDateRange dateRangeWithDayDate:today startHour:9 startMinute:0 endHour:13 endMinute:30]"];

    [logStr appendFunctionHeader:@"Description"];
    
    [logStr append:@"a_daterange : %@",[dr description]];
    [logStr append:@"a_daterange_today : %@",[dr2 description]];
    
    [logStr append:@"a_daterange.duration : %.2f",[dr duration]];
    [logStr append:@"a_daterange_today.duration : %.2f",[dr2 duration]];
    
    [logStr appendFunctionHeader:@"Test"];

    [logStr append:@"Is NOW in range a_daterange: %@",MF_YesNo( [dr containsDate:today] )];
     [logStr append:@"Is NOW in range a_daterange_today: %@",MF_YesNo( [dr2 containsDate:today] )];

    /*
    -(BOOL)containsDayWithComponents:(NSDateComponents*)comps inCalendar:(NSCalendar*)cal;
     
    -(void)startHour:(NSUInteger*)hours minutes:(NSUInteger*)minutes;
    -(void)endHour:(NSUInteger*)hours minutes:(NSUInteger*)minutes;
     */
    
     NSLog(@"%@",logStr);

}

@end
